from Trace import Trace
from PyQt5 import QtGui

from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *


class CanvasDessin(QWidget):

    def __init__(self):
        super().__init__()
        self.traces = list()
        self.actualTraces = list()
        self.setMouseTracking(True)
        self.setMinimumSize(1000, 600)
        self.cursorPos = None
        self.trace = Trace()
        self.width = self.trace.getWidth()
        self.color = self.trace.getColor()

    def mousePressEvent(self, event):
        self.trace = Trace()
        self.trace.color = self.color
        self.trace.width = self.width
        self.trace.addPoints(event.pos())
        self.traces.append(self.trace)
        self.update()

    def mouseMoveEvent(self, event):
        if (self.trace != None):
            self.trace.addPoints(event.pos())
        self.update()

    def mouseReleaseEvent(self, event):
        self.trace.addPoints(event.pos())
        self.trace = None
        self.update()

    def setWidth(self, width):
            self.width = width

    def setColor(self, color):
            self.color = QColor(color)

    def paintEvent(self, event):
        painter = QPainter(self)
        for trace in self.traces:
            pen = QPen()
            pen.setWidth(trace.width)
            pen.setBrush(trace.color)
            painter.setPen(pen)
            path = QPainterPath()
            path.moveTo(trace.points[0])
            for point in range(1, len(trace.points)):
                path.lineTo(trace.points[point])
                painter.drawPath(path)