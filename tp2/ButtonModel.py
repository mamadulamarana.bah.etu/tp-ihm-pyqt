
class ButtonModel():
    idle = 0
    hover = 1
    pressIn = 2
    pressOut = 3

    def __init__(self):
        self.state = ButtonModel.idle

    def getState(self):
        return self.state

    def Enter(self):
        print("press enter")
        if (self.state == ButtonModel.idle):
            self.state = ButtonModel.hover

        elif(self.state == ButtonModel.pressOut):
            self.state = ButtonModel.pressIn

    def Leave(self):
        print("Leave press")
        if(self.state == ButtonModel.hover):
            self.state = ButtonModel.idle
        elif(self.state == ButtonModel.pressIn):
            self.state = ButtonModel.pressOut

    def Release(self):
        print("Release press")
        if(self.state == ButtonModel.pressOut):
            self.state = ButtonModel.idle
        elif(self.state == ButtonModel.pressIn):
            self.action()
            self.state = ButtonModel.hover

    def Press(self):
        print("Press press")
        if(self.state == ButtonModel.hover):
            self.state = ButtonModel.pressIn

    def action(self):
        print("etat final : action")