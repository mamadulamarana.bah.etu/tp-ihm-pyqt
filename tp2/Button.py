import sys
from PyQt5 import QtGui

from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *

from ButtonModel import ButtonModel

class CanvasButton(QWidget):

  defaultCol = QColor("blue")
  hoverCol = QColor("yellow")
  pressCol = QColor("red")

  def __init__(self):
    super().__init__()
    self.bbox = QRect(100, 100, 200, 200)
    self.setMouseTracking(True)
    self.cursorOver = False
    self.buttonModel = ButtonModel()
    self.cursorPos = None

  def mouseMoveEvent(self, event):
    print("mouse event")
    self.cursorPos = event.pos()
    if (self.cursorOnEllipse(self.cursorPos) ):
      self.cursorOver = True
      self.buttonModel.Enter()
    else:
      self.cursorOver = False
      self.buttonModel.Leave()
    self.update()
    
  def mousePressEvent(self, event):
    print("mouse Press")
    if (self.cursorOver):
      self.buttonModel.Press()
    self.update()

  def mouseReleaseEvent(self, event):
    print("mouse release")
    self.buttonModel.Release()
    self.update()

  def paintEvent(self, event):
    painter = QPainter(self)
    painter.setPen(CanvasButton.defaultCol)
    painter.setBrush(CanvasButton.defaultCol)
    if ( self.buttonModel.state == ButtonModel.idle or self.buttonModel.state == ButtonModel.pressOut ):
      painter.setBrush(CanvasButton.defaultCol)
    elif(self.buttonModel.state == ButtonModel.hover):
      painter.setBrush(CanvasButton.hoverCol)
    elif(self.buttonModel.state == ButtonModel.pressIn):
      painter.setBrush(CanvasButton.pressCol)

    painter.drawEllipse(self.bbox)

    if (self.buttonModel.state == ButtonModel.pressIn or self.buttonModel.state == ButtonModel.pressOut):
      painter.setBrush(QColor(0, 0, 0, 0))
      painter.drawEllipse(\
        self.cursorPos.x()-45,\
        self.cursorPos.y()-45, 50, 50)

  def cursorOnEllipse(self, point):
    return self.bbox.contains(point)


class MainWindow(QMainWindow):

  def __init__(self):
    super().__init__()

def main(args):
  app = QApplication(args)
  m = QMainWindow()
  m.resize(1000, 1000)
  canvasButton = CanvasButton()
  m.setCentralWidget(canvasButton)



  m.show()
  app.exec()

if __name__ == '__main__':
  print("execution du programme")
  main(sys.argv)