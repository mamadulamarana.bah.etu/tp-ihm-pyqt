from PyQt5 import QtGui
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *

class Trace() :

    def __init__(self, width=30, color=QColor("blue")):
        self.points = list()
        self.width = width
        self.color = color

    def addPoints(self, point):
        self.points.append(point)

    def getWidth(self):
        return self.width
    
    def getColor(self):
        return self.color
