import sys
from CanvasDessin import CanvasDessin
from PyQt5 import QtGui

from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *


class Dessin(QMainWindow):

    def __init__(self, canvas):
        super().__init__()
        self.canvasDessin = canvas
        self.setCentralWidget(self.canvasDessin)

    def modifyColor(self):
        color = QColorDialog(self).getColor()
        self.canvasDessin.setColor(color)

    def modifyWidth(self, value):
         width = value
         self.canvasDessin.setWidth(width)

    def resetDessin(self):
         self.canvasDessin.traces = []
         self.update()

def main(args):
    app = QApplication(args)
    canvas = CanvasDessin()
    m = Dessin(canvas)

    toolBar = QToolBar("Edit", m)

    modifyColor = QAction("Color", m)
    toolBar.addAction(modifyColor)
    modifyColor.triggered.connect(m.modifyColor)

    reset = QPushButton("Reset", m)
    toolBar.addWidget(reset)
    reset.clicked.connect(m.resetDessin)

    #result_label = QLabel('', m)
    slider = QSlider(Qt.Orientation.Horizontal)
    slider.setRange(0, 100)
    slider.setValue(30)
    slider.valueChanged.connect(m.modifyWidth)
    #result_label.setText(f'width : {m.width}')
    toolBar.addWidget(slider)
    #toolBar.addWidget(result_label)

    m.addToolBar(toolBar)
    m.show()
    app.exec()


if __name__ == "__main__":
	print("execution du programme")
	main(sys.argv) 