import sys
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *

class MainWindow(QMainWindow) :
	def __init__(self):

		super().__init__()
		self.edit = QTextEdit()
		self.setQtextEdit() # initialisation du QTextEdit pour Q6

	def setQtextEdit(self):
		self.edit = QTextEdit(self)
		self.setCentralWidget(self.edit)		

	def openFile(self):
		fileName, _ = QFileDialog.getOpenFileName(self, "open file", "tp1/", "*.txt *.html")
		print(str(fileName))
		file = QFile()
		file.setFileName(fileName)
		if (file.open(QFile.ReadOnly | QFile.Text)) :
			stream = QTextStream(file)
			content = stream.readAll()
			self.edit.setHtml(content)
			file.close()

	def saveFile(self):
		fileName, _ = QFileDialog.getSaveFileName(self, "save file", "tp1/", "*.html *.txt")
		print(fileName)
		if ( len(fileName) > 0):
			file = QFile()
			file.setFileName(fileName)
			if (file.open(QFile.WriteOnly | QFile.Text)):
				stream = QTextStream(file)
				content = self.edit.toHtml()
				stream << content
				file.close()

	def quitApp(self):
		print("quit ?")
		message = QMessageBox()
		message.setIcon(QMessageBox.Information)
		message.setText("Voulez-vous quittez ?")
		message.setWindowTitle("Quit")
		message.setStandardButtons(QMessageBox.Yes | QMessageBox.No)
		value = message.exec()
		if value == QMessageBox.Yes:
			sys.exit()

	def closeEvent(self, event):
		event.ignore()
		self.quitApp()

		
def main(args):
	print(args)

	app = QApplication(args)
	m = MainWindow()
	m.resize(1000, 1000)

	bar = m.menuBar()
	statusBar = m.statusBar()

	fileMenu = bar.addMenu("Fichier")

	newAct1 = QAction(QIcon("open.png"), "Open...", m)
	newAct2 = QAction(QIcon("save.png"), "Save...", m)
	newAct3 = QAction(QIcon("copy.png"), "Copy...", m)
	newAct4 = QAction(QIcon("quit.png"), "Quit...", m)

	newAct1.setToolTip("open")
	newAct2.setToolTip("save")
	newAct1.setStatusTip("open file")
	newAct2.setStatusTip("save a file")
	newAct3.setStatusTip("copy file")
	newAct4.setStatusTip("quit and close program")
	newAct1.setShortcut("Ctrl+O")
	newAct2.setShortcut("Ctrl+s")
	newAct3.setShortcut("Ctrl+c")
	newAct4.setShortcut("Ctrl+q")

	fileToolBar = m.addToolBar("Fichier")
	fileToolBar.addAction(newAct1)
	fileToolBar.addAction(newAct2)
	fileToolBar.addAction(newAct3)
	fileToolBar.addAction(newAct4)

	newAct1.triggered.connect(m.openFile)
	newAct2.triggered.connect(m.saveFile)
	newAct4.triggered.connect(m.quitApp)

	fileMenu.addAction(newAct1)
	fileMenu.addAction(newAct2)
	fileMenu.addAction(newAct3)
	fileMenu.addAction(newAct4)

	m.show()
	app.exec()	

if __name__ == "__main__":
	print("execution du programme")
	main(sys.argv) 